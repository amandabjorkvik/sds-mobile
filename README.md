# Tomato Timer

Tomato Timer is a timer app for studying. Enter for how long you want to study and as time moves on, the 
countdown will make a tomato.

If you want a preview of the how the app works, go to the file 'video' and watch the YouTube video.

This Android app is a school project for the course Software Development Skills: Mobile.

## Installation

Clone this repository and import the folder 'Project' into Android Studio.

```bash
git clone https://amandabjorkvik@bitbucket.org/amandabjorkvik/sds-mobile.git
```

## Maintainers

This project is maintained by Amanda Bjorkvik.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.