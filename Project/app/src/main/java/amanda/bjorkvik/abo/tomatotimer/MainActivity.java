package amanda.bjorkvik.abo.tomatotimer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static long startTimeInMilliseconds = 1500000;
    private static TextView countdownTextView;
    private static Button startPauseBtn;
    private static Button resetBtn;
    private static CountDownTimer countDownTimer;
    private static boolean timerRunning;
    private static long timeLeftInMilliseconds = startTimeInMilliseconds;
    private FloatingActionButton settingsFloatingActionBtn;
    private static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countdownTextView = findViewById(R.id.countdownTextView);
        startPauseBtn = findViewById(R.id.startPauseBtn);
        resetBtn = findViewById(R.id.resetBtn);
        settingsFloatingActionBtn = findViewById(R.id.settingsFloatingActionBtn);
        progressBar = findViewById(R.id.progressBar);

        settingsFloatingActionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSettings();
            }
        });

        // help from https://codinginflow.com/tutorials/android/countdowntimer/part-1-countdown-timer
        startPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timerRunning) {
                    pauseTimer();
                }
                else {
                    startTimer();
                }
            }
        });

        // help from https://codinginflow.com/tutorials/android/countdowntimer/part-1-countdown-timer
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
            }
        });
    }

    // help from https://codinginflow.com/tutorials/android/countdowntimer/part-1-countdown-timer
    private void startTimer () {
        countDownTimer = new CountDownTimer(timeLeftInMilliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMilliseconds = millisUntilFinished;
                updateCountdown();
            }

            @Override
            public void onFinish() {
                timerRunning = false;
                updateButtons();
            }
        }.start();

        timerRunning = true;
        updateButtons();
        updateCountdown();
    }

    // help from https://codinginflow.com/tutorials/android/countdowntimer/part-1-countdown-timer
    private void pauseTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        timerRunning = false;
        updateButtons();
    }

    // help from https://codinginflow.com/tutorials/android/countdowntimer/part-1-countdown-timer
    public static void resetTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        timerRunning = false;
        timeLeftInMilliseconds = startTimeInMilliseconds;
        updateCountdown();
        updateButtons();
    }

    // help from https://codinginflow.com/tutorials/android/countdowntimer/part-1-countdown-timer
    private static void updateCountdown() {
        int minutes = (int) ((timeLeftInMilliseconds/1000)/60);
        int seconds = (int) ((timeLeftInMilliseconds/1000)%60);
        String timeLeftFormatted = String.format(Locale.getDefault(),"%02d:%02d", minutes, seconds);
        countdownTextView.setText(timeLeftFormatted);
        updateProgress();
    }

    // help from https://codinginflow.com/tutorials/android/countdowntimer/part-3-run-in-background
    private static void updateButtons() {
        if (timerRunning) {
            resetBtn.setVisibility(View.INVISIBLE);
            startPauseBtn.setText("Pause");
        }
        else {
            startPauseBtn.setText("Start");
            if (timeLeftInMilliseconds < 1000) {
                startPauseBtn.setVisibility(View.INVISIBLE);
            }
            else {
                startPauseBtn.setVisibility(View.VISIBLE);
            }
            if (timeLeftInMilliseconds < startTimeInMilliseconds) {
                resetBtn.setVisibility(View.VISIBLE);
            }
            else {
                resetBtn.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void openSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public static void updateTime(long milliseconds) {
        startTimeInMilliseconds = milliseconds;
    }

    // help from https://codinginflow.com/tutorials/android/circular-determinate-progressbar-with-background-and-text
    private static void updateProgress() {
        double startTime = startTimeInMilliseconds;
        double timeLeft = timeLeftInMilliseconds;
        int currentProgress;
        if (timeLeft < 1000) {
            currentProgress = 100;
        }
        else {
            currentProgress = (int) (100 - ((timeLeft / startTime) * 100));
        }
        progressBar.setProgress(currentProgress);
    }
}