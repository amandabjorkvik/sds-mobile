package amanda.bjorkvik.abo.tomatotimer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    private EditText setMinutesEditText;
    private Button setMinutesBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setMinutesEditText = findViewById(R.id.setMinutesEditText);
        setMinutesBtn = findViewById(R.id.setMinutesBtn);

        // help from https://codinginflow.com/tutorials/android/countdowntimer/part-4-time-input
        setMinutesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = setMinutesEditText.getText().toString();
                if (input.length() == 0) {
                    Toast.makeText(SettingsActivity.this, "Field can not be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                long inputInMilliseconds = (Long.parseLong(input) * 60000);
                if (inputInMilliseconds == 0) {
                    Toast.makeText(SettingsActivity.this, "Please enter a positive number", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (inputInMilliseconds > 3600000) {
                    Toast.makeText(SettingsActivity.this, "You can not enter more than 60 minutes", Toast.LENGTH_SHORT).show();
                    return;
                }
                setTime(inputInMilliseconds);
                setMinutesEditText.setText("");
            }
        });
    }

    // help from https://codinginflow.com/tutorials/android/countdowntimer/part-4-time-input
    private void setTime(long milliseconds) {
        MainActivity.updateTime(milliseconds);
        MainActivity.resetTimer();
    }

}